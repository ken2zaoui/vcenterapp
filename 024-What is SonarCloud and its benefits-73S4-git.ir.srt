1
00:00:00,0 --> 00:00:10,0
--- GIT.IR e-Learning Platform ---

1
00:00:00,270 --> 00:00:01,610
‫Hello, security gurus.

2
00:00:01,620 --> 00:00:03,190
‫Welcome to this new lecture.

3
00:00:03,210 --> 00:00:08,520
‫In this lecture, we are going to understand what is sonar cloud and what are its benefits.

4
00:00:08,520 --> 00:00:15,720
‫So so now cloud is a software as a service platform that helps us to keep our source code free from

5
00:00:15,720 --> 00:00:20,760
‫any kind of code quality issues, any kind of code security issues.

6
00:00:20,760 --> 00:00:30,240
‫It also helps us to define quality gates within the sonar cloud so that we can set our standards as

7
00:00:30,240 --> 00:00:33,900
‫per the project or organization quality standards.

8
00:00:33,900 --> 00:00:41,400
‫For an example, sonar cloud provides a default quality gate where code coverage is defined as 80%.

9
00:00:41,400 --> 00:00:49,740
‫But suppose in our organization or in our project, we maintain 90% code coverage or unit test coverage

10
00:00:49,740 --> 00:00:51,720
‫for any code that we write.

11
00:00:51,750 --> 00:00:56,880
‫Then we can define our custom quality gates within sonar cloud.

12
00:00:56,880 --> 00:01:00,990
‫So sonar cloud has very similar functionality with sonar.

13
00:01:00,990 --> 00:01:08,430
‫Q The only difference is that sonar generally we install it on our local system or we install it as

14
00:01:08,430 --> 00:01:12,870
‫a software on our on premises server or on cloud.

15
00:01:12,870 --> 00:01:20,520
‫But sonar cloud has already been installed on our cloud and has been provided as a service so that you

16
00:01:20,520 --> 00:01:29,100
‫can directly integrate your build system with the sonar cloud using its APIs and you are good to go

17
00:01:29,100 --> 00:01:34,530
‫and you can start running quality checks on the source code that your developers are writing.

18
00:01:34,530 --> 00:01:38,240
‫So this is what sonar cloud is by a security guru.

19
00:01:38,250 --> 00:01:40,590
‫I hope you have enjoyed this lecture.

20
00:01:40,590 --> 00:01:41,760
‫Thanks for watching.

21
00:01:41,790 --> 00:01:43,410
‫See you in the next lecture.


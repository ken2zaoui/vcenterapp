1
00:00:00,0 --> 00:00:10,0
--- GIT.IR e-Learning Platform ---

1
00:00:00,540 --> 00:00:01,860
‫Hello, security gurus.

2
00:00:01,860 --> 00:00:03,520
‫Welcome to this new lecture.

3
00:00:03,540 --> 00:00:09,840
‫In this lecture, we are going to integrate software composition analysis scan within our Devsecops

4
00:00:09,840 --> 00:00:10,410
‫pipeline.

5
00:00:10,410 --> 00:00:16,950
‫Using Jenkins, we are using Snake for running software composition analysis can on our source code.

6
00:00:17,070 --> 00:00:23,730
‫See C scan is used to find security vulnerabilities within third party libraries that we define within

7
00:00:24,480 --> 00:00:26,670
‫example in case of a Java project.

8
00:00:26,670 --> 00:00:32,400
‫So let's see the report that we have created since we have added changes to our repo.

9
00:00:32,400 --> 00:00:34,740
‫I have changed the name of the repo here.

10
00:00:34,740 --> 00:00:39,960
‫As you can see, that is Sast and SQL present in the repo as well.

11
00:00:39,960 --> 00:00:44,640
‫And that's why you can see the names on our cloud and snake repo after that.

12
00:00:44,640 --> 00:00:47,790
‫I have made a few changes in our repo.

13
00:00:47,790 --> 00:00:54,570
‫The first change that I've done is, in example, I've added a plugin, let's see that plugin.

14
00:00:54,570 --> 00:00:58,110
‫I'll scroll down and I'll zoom in.

15
00:00:59,260 --> 00:01:04,320
‫As you can see that I have added this plugin change to our XML.

16
00:01:04,330 --> 00:01:08,950
‫This is a sneak plugin as you can see, and it's specially created for Maven.

17
00:01:08,980 --> 00:01:15,170
‫I have given the sneak organization name that we have created, which is a security group.

18
00:01:15,190 --> 00:01:22,720
‫So this plugin change we need to add in order to run snake scan for software composition analysis on

19
00:01:22,720 --> 00:01:23,510
‫our source code.

20
00:01:23,530 --> 00:01:26,830
‫This is the first change that I've made to our repo.

21
00:01:26,860 --> 00:01:28,120
‫Let's go back.

22
00:01:29,230 --> 00:01:32,650
‫The second change that I've done is within Jenkins file.

23
00:01:32,680 --> 00:01:34,180
‫Let's open it.

24
00:01:35,270 --> 00:01:36,500
‫I'll scroll down.

25
00:01:36,530 --> 00:01:42,330
‫Now, within Jenkins file, I've added another stage here within our Devsecops pipeline.

26
00:01:42,350 --> 00:01:47,840
‫The stage name is Run C analysis using snake, where we have written some steps.

27
00:01:47,840 --> 00:01:51,440
‫The first step is that we are using with credentials.

28
00:01:51,440 --> 00:01:58,610
‫Key word of Jenkins with credentials is used when we have stored some credentials within Jenkins credential

29
00:01:58,610 --> 00:02:03,470
‫manager and we are using those credentials within our pipeline.

30
00:02:03,470 --> 00:02:10,910
‫So we will define this as we will have to create a sneak token in Jenkins Credential Manager That's

31
00:02:10,910 --> 00:02:14,630
‫why I have added this with credentials keyword.

32
00:02:14,630 --> 00:02:20,960
‫We are adding a sneak underscore token credential that we need to create in Jenkins credential manager

33
00:02:20,970 --> 00:02:21,770
‫will do that.

34
00:02:21,770 --> 00:02:27,560
‫And then we are running a shell script where we are running Maven using HM command and we are running

35
00:02:27,560 --> 00:02:29,660
‫snake test on our source code.

36
00:02:29,660 --> 00:02:34,220
‫We are using hyphen f in flag in order to avoid any build failures.

37
00:02:34,220 --> 00:02:40,430
‫If we do not add this flag, our build will fail because snake will find security issues in the source

38
00:02:40,430 --> 00:02:45,830
‫code as it is a vulnerable application and there are many third party libraries which are vulnerable,

39
00:02:45,830 --> 00:02:47,540
‫so our build will fail.

40
00:02:47,570 --> 00:02:51,260
‫Hence, to avoid any build failures, I've added this hyphen in flag.

41
00:02:51,260 --> 00:02:58,790
‫No, let's create a snake token in order to create a sneak token and save it in Jenkins Credential Manager.

42
00:02:58,790 --> 00:03:01,940
‫First, we need to generate it from Snake Dashboard.

43
00:03:01,940 --> 00:03:04,340
‫So I'll open the Snake Dashboard here.

44
00:03:04,340 --> 00:03:05,540
‫I'll open a new tab.

45
00:03:05,540 --> 00:03:06,950
‫I'll go to Snake.

46
00:03:08,500 --> 00:03:10,210
‫I log in with the snake.

47
00:03:10,390 --> 00:03:16,060
‫As you can see, I have logged in within snake and you can see that this is our organisation, which

48
00:03:16,060 --> 00:03:17,480
‫is a security group.

49
00:03:17,500 --> 00:03:23,830
‫You can see that in the URL and you can see that this is our snake organization that gets created once

50
00:03:23,830 --> 00:03:25,570
‫we create an account with Snake.

51
00:03:25,600 --> 00:03:31,930
‫Now, in order to create a security token, we will have to click on our profile icon and then go to

52
00:03:31,930 --> 00:03:33,190
‫account settings.

53
00:03:34,510 --> 00:03:40,810
‫And then we will have to click in this text box where they have written it's an authorization token

54
00:03:40,810 --> 00:03:44,850
‫and used to authenticate with Snake Clay in CD Pipeline.

55
00:03:44,860 --> 00:03:49,690
‫So click on this text box and you will be able to see the authentication token.

56
00:03:49,690 --> 00:03:51,130
‫Let's copy it.

57
00:03:51,730 --> 00:03:54,160
‫Now let's go back to Jenkins.

58
00:03:55,120 --> 00:03:59,020
‫Now I'll scroll up, I'll go to Jenkins dashboard.

59
00:03:59,650 --> 00:04:04,600
‫I'll go to manage Jenkins, I'll go to manage credentials.

60
00:04:05,960 --> 00:04:07,580
‫I'll click on Jenkins.

61
00:04:09,410 --> 00:04:11,630
‫I'll click on Global Credentials.

62
00:04:12,840 --> 00:04:13,350
‫No.

63
00:04:13,350 --> 00:04:15,660
‫I will click on add credentials.

64
00:04:17,150 --> 00:04:21,560
‫I will select a dropdown menu which is secret text.

65
00:04:21,560 --> 00:04:25,580
‫In this case, I'll paste the secret.

66
00:04:26,500 --> 00:04:32,620
‫It is sneak token and we will provide the same name that we have defined in Jenkins file, which is

67
00:04:32,620 --> 00:04:34,290
‫snake underscore token.

68
00:04:34,300 --> 00:04:38,240
‫Now we will cooperate and paste it in the description as well.

69
00:04:38,260 --> 00:04:42,280
‫One thing to notice here is that we need to define the sneak token.

70
00:04:42,280 --> 00:04:49,600
‫In same order it shall be in capital letters with sneak underscore token because this is the guideline

71
00:04:49,600 --> 00:04:51,070
‫provided by Snake.

72
00:04:51,580 --> 00:04:53,260
‫Let's click on Create Button.

73
00:04:54,620 --> 00:04:57,480
‫Now we have added this sneak token successfully.

74
00:04:57,500 --> 00:04:59,810
‫Let's go back to Jenkins dashboard.

75
00:05:00,680 --> 00:05:01,430
‫No.

76
00:05:01,430 --> 00:05:08,390
‫This was the previous pipeline that we created for Sonar Cloud since we have added another step to the

77
00:05:08,390 --> 00:05:11,810
‫previous pipeline and its use has changed.

78
00:05:11,840 --> 00:05:15,500
‫What we will do is we will update this pipeline job.

79
00:05:15,500 --> 00:05:18,980
‫So in order to update this pipeline job, let's click on it.

80
00:05:20,200 --> 00:05:21,910
‫Let's go to configure.

81
00:05:24,070 --> 00:05:25,780
‫Now let's scroll down.

82
00:05:26,730 --> 00:05:31,740
‫Now we need to update this URL to the new report that we have created.

83
00:05:31,770 --> 00:05:33,870
‫I'll go back to our GitHub repo.

84
00:05:33,900 --> 00:05:35,180
‫I'll scroll up.

85
00:05:35,190 --> 00:05:36,900
‫I'll go back to code.

86
00:05:37,080 --> 00:05:38,610
‫I'll click on it now.

87
00:05:38,610 --> 00:05:40,080
‫I'll click on code again.

88
00:05:40,110 --> 00:05:42,390
‫Now let's copy this URL.

89
00:05:42,420 --> 00:05:49,170
‫Now go back to Jenkins and replace the URL with the new report that we have created.

90
00:05:49,650 --> 00:05:57,060
‫Now, this is the repo that we have created for integrating sast and see steps within our ops pipeline.

91
00:05:57,480 --> 00:05:57,900
‫Rest.

92
00:05:57,900 --> 00:05:59,550
‫Everything will remain the same.

93
00:05:59,580 --> 00:06:01,020
‫Let's click on Save.

94
00:06:02,830 --> 00:06:05,140
‫And let's click on Build now again.

95
00:06:07,730 --> 00:06:09,550
‫Now our bill is running.

96
00:06:09,560 --> 00:06:13,800
‫Now we have integrated two steps within our Devsecops pipeline.

97
00:06:13,820 --> 00:06:20,130
‫The first step is that the static application security testing scan will be done on our source code.

98
00:06:20,150 --> 00:06:27,080
‫Once that scan is completed, then software composition analysis can will be done on our source code

99
00:06:27,080 --> 00:06:28,250
‫using sneak.

100
00:06:28,250 --> 00:06:31,640
‫Let's click on the job number to see the logs.

101
00:06:32,560 --> 00:06:36,040
‫Let's click on console output as you can see.

102
00:06:36,040 --> 00:06:38,310
‫So now the analysis is currently running.

103
00:06:38,320 --> 00:06:40,240
‫Let's wait for a few seconds.

104
00:06:41,420 --> 00:06:42,320
‫As you can see.

105
00:06:42,320 --> 00:06:48,440
‫So now the analysis is complete and now Snake Clay is being downloaded, as you can see.

106
00:06:48,470 --> 00:06:51,110
‫I'll zoom in so that you can see better.

107
00:06:52,740 --> 00:06:54,120
‫I'll scroll down.

108
00:06:54,150 --> 00:07:02,340
‫As you can see, Snake Clay is being downloaded and it has been downloaded and Snake has already identified

109
00:07:02,340 --> 00:07:07,050
‫42 issues within our source code for third party libraries.

110
00:07:07,080 --> 00:07:15,240
‫If I scroll down, we will see that build has failed because Snake has identified security issues within

111
00:07:15,240 --> 00:07:16,770
‫the third party libraries.

112
00:07:16,770 --> 00:07:21,900
‫But still our Jenkins build has passed or it's successful.

113
00:07:21,930 --> 00:07:28,950
‫The reason is that we have used hyphen in flag within our Jenkins file to make sure that our build does

114
00:07:28,950 --> 00:07:30,360
‫not fail to conform.

115
00:07:30,360 --> 00:07:37,540
‫We can open the build in a new tab and you can verify that the build is successful.

116
00:07:37,560 --> 00:07:44,400
‫The build was number two and you can see that run C analysis using snake is complete as well and it's

117
00:07:44,400 --> 00:07:45,570
‫marked in green.

118
00:07:45,600 --> 00:07:48,590
‫Now let's see the logs generated by Snake.

119
00:07:48,600 --> 00:07:49,800
‫I'll scroll up.

120
00:07:50,130 --> 00:07:51,000
‫I'll zoom in.

121
00:07:51,000 --> 00:07:57,360
‫And as you can see, Snake tested 35 dependencies for known issues and found 42 issues.

122
00:07:57,360 --> 00:08:02,010
‫So it means that there are multiple vulnerabilities within a single library.

123
00:08:02,040 --> 00:08:06,780
‫Snake is suggesting that some issues can be fixed by just upgrading the libraries.

124
00:08:06,780 --> 00:08:16,350
‫For an example, if we upgrade my SQL connector from 5.1. to five version to 8.0.28 version, it will

125
00:08:16,350 --> 00:08:23,970
‫fix all these issues which are improper access, control, improper authorization, examine external

126
00:08:23,970 --> 00:08:30,000
‫entity, injection privilege, escalation, code execution and many others.

127
00:08:30,000 --> 00:08:35,820
‫Also, you can see that snake has identified the severity of this issue, whether it's a medium severity

128
00:08:35,820 --> 00:08:37,080
‫or low severity issue.

129
00:08:37,080 --> 00:08:41,970
‫So that's how you integrate Snake within the Devsecops pipeline.

130
00:08:41,970 --> 00:08:47,670
‫Now we have integrated Sast and Snake within our Devsecops pipeline.

131
00:08:47,670 --> 00:08:53,790
‫In the upcoming lectures, we will add more tools in the pipeline and we will move towards an end to

132
00:08:53,790 --> 00:08:56,370
‫end Devsecops pipeline using Jenkins.

133
00:08:56,370 --> 00:08:57,430
‫So stay tuned.

134
00:08:57,450 --> 00:08:58,530
‫Thanks for watching.

135
00:08:58,560 --> 00:09:00,180
‫See you in the next lecture.


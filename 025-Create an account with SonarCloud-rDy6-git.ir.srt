1
00:00:00,0 --> 00:00:10,0
--- GIT.IR e-Learning Platform ---

1
00:00:00,330 --> 00:00:01,800
‫Hello, security gurus.

2
00:00:01,800 --> 00:00:03,360
‫Welcome to this new lecture.

3
00:00:03,390 --> 00:00:07,290
‫In this lecture, we are going to create an account on sonar cloud.

4
00:00:07,290 --> 00:00:09,000
‫So let's jump into the action.

5
00:00:09,000 --> 00:00:14,730
‫In order to create an account on sonar cloud, we need to visit Sonar Cloud dot IO website.

6
00:00:14,760 --> 00:00:16,530
‫This is their landing page.

7
00:00:16,530 --> 00:00:22,830
‫When we are on the landing page, we need to click on the login button here, so I'll click on it.

8
00:00:25,580 --> 00:00:32,360
‫As soon as I click on the login button, I'll be routed to this web page which says login or sign up

9
00:00:32,360 --> 00:00:39,350
‫to SoundCloud using these options, which are with GitHub, with Bitbucket, with GitLab, or with Azure

10
00:00:39,350 --> 00:00:40,220
‫DevOps account.

11
00:00:40,250 --> 00:00:47,240
‫Now, since I have already an account with GitHub, I will select this option as it is the easiest option

12
00:00:47,240 --> 00:00:47,840
‫for me.

13
00:00:47,840 --> 00:00:49,570
‫So let's click on this.

14
00:00:49,580 --> 00:00:51,650
‫I'll click on with GitHub button.

15
00:00:54,150 --> 00:01:00,900
‫As soon as I clicked on with GitHub button, I was routed to this page where it is asking me to enter

16
00:01:00,900 --> 00:01:04,260
‫my username and password for my GitHub account.

17
00:01:04,290 --> 00:01:09,570
‫Now for my GitHub account, my username is a security guru and this is my password.

18
00:01:09,570 --> 00:01:10,820
‫I have entered it.

19
00:01:10,830 --> 00:01:12,960
‫Let's click on sign in button.

20
00:01:15,570 --> 00:01:21,870
‫As soon as I clicked on Sign In Button, it says that you are authorized and you are being routed to

21
00:01:21,870 --> 00:01:23,660
‫this dashboard.

22
00:01:23,700 --> 00:01:29,010
‫As soon as I clicked on sign in button, I was routed to our Cloud Dashboard page.

23
00:01:29,040 --> 00:01:35,580
‫As you can see, our account has been created within our Cloud Dashboard and there were a few projects

24
00:01:35,580 --> 00:01:38,700
‫which I had already created within Sundar Cloud.

25
00:01:38,700 --> 00:01:42,160
‫So that's why we are able to see these two projects here.

26
00:01:42,180 --> 00:01:47,040
‫In your case, you won't be able to see any project here in the next lectures.

27
00:01:47,040 --> 00:01:50,040
‫We will try to create some projects in Sundar Cloud.

28
00:01:50,040 --> 00:01:53,820
‫So this was all about creating an account with Sundar Cloud.

29
00:01:53,820 --> 00:01:55,650
‫I hope you enjoyed this lecture.

30
00:01:55,650 --> 00:01:56,640
‫Thanks for watching.

31
00:01:56,670 --> 00:01:58,220
‫See you in the next lecture.


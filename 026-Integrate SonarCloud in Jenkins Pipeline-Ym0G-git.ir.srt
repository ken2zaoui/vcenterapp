1
00:00:00,0 --> 00:00:10,0
--- GIT.IR e-Learning Platform ---

1
00:00:00,330 --> 00:00:01,740
‫Hello, security gurus.

2
00:00:01,740 --> 00:00:03,250
‫Welcome to this new lecture.

3
00:00:03,270 --> 00:00:09,630
‫In this lecture we are going to implement our Devsecops pipeline where we will integrate sooner cloud

4
00:00:09,630 --> 00:00:11,310
‫within our Devsecops pipeline.

5
00:00:11,310 --> 00:00:17,850
‫Using Jenkins for this demonstration purpose, we will use Jenkins declarative pipeline and we will

6
00:00:17,850 --> 00:00:20,250
‫create this pipeline in this lecture.

7
00:00:20,250 --> 00:00:22,260
‫So let's jump into the action.

8
00:00:22,260 --> 00:00:30,090
‫I am on GitHub Dotcom and I've opened my repo devsecops Jenkins Kubernetes with TerraForm and so now

9
00:00:30,090 --> 00:00:30,510
‫Cloud.

10
00:00:30,510 --> 00:00:34,620
‫So this is the repo which we are going to use in this repo.

11
00:00:34,650 --> 00:00:42,300
‫I have already written the code for integrating sooner cloud within Jenkins for implementing our Devsecops

12
00:00:42,300 --> 00:00:43,020
‫pipeline.

13
00:00:43,020 --> 00:00:48,600
‫Now this is an easy, buggy, vulnerable web app modified by a security guru.

14
00:00:48,600 --> 00:00:51,240
‫So this is a vulnerable web application.

15
00:00:51,240 --> 00:00:57,810
‫As you can see here, I have cloned this repo and I have modified it for a demonstration purpose.

16
00:00:57,810 --> 00:01:04,980
‫There is already a docker file present in this repo as well and I have written a Jenkins file for implementing

17
00:01:04,980 --> 00:01:07,410
‫our Devsecops pipeline within Jenkins.

18
00:01:07,410 --> 00:01:13,830
‫Now let's open the Jenkins file to see the code that we have written for implementing Devsecops pipeline

19
00:01:13,830 --> 00:01:17,010
‫and for integrating Cloud within Jenkins.

20
00:01:17,010 --> 00:01:18,030
‫I'll click on it.

21
00:01:19,900 --> 00:01:26,380
‫Now within Jenkins file, we have written a pipeline syntax, which is also called Declarative Syntax

22
00:01:26,380 --> 00:01:27,320
‫of Jenkins.

23
00:01:27,340 --> 00:01:30,310
‫Within that, we have started with pipeline keyword.

24
00:01:30,340 --> 00:01:33,070
‫After that, we have written agent any.

25
00:01:33,100 --> 00:01:37,000
‫Agent any means that Jenkins agent should be able to run this code.

26
00:01:37,030 --> 00:01:43,480
‫After that, we have defined some tools and within tools we have defined MAVEN underscore three, underscore

27
00:01:43,480 --> 00:01:44,750
‫five, underscore two.

28
00:01:44,770 --> 00:01:51,340
‫This is the same Maven name that we gave in the previous lecture while configuring Maven in Jenkins.

29
00:01:51,370 --> 00:01:57,610
‫After that, we have defined stages within Jenkins file stages that we are going to define within our

30
00:01:57,610 --> 00:01:58,840
‫DEVSECOPS pipeline.

31
00:01:58,840 --> 00:02:04,690
‫Within these stages, we are first going to define a first stage which is compile and run through our

32
00:02:04,690 --> 00:02:05,450
‫analysis.

33
00:02:05,470 --> 00:02:10,240
‫It means that we are going to run sonar analysis on our source code.

34
00:02:10,270 --> 00:02:12,430
‫Now let's edit this file.

35
00:02:13,280 --> 00:02:17,390
‫I'll select the soft wrap so that we can see the entire code.

36
00:02:17,420 --> 00:02:18,620
‫I'll zoom in.

37
00:02:18,650 --> 00:02:19,780
‫I'll scroll down.

38
00:02:19,790 --> 00:02:22,190
‫Now you can see the entire syntax.

39
00:02:22,220 --> 00:02:29,270
‫So first we configured Maven and now we are going to run, compile and run sonar analysis.

40
00:02:29,300 --> 00:02:34,190
‫Now we have used stages keyword to define the stages of our devsecops.

41
00:02:34,190 --> 00:02:39,260
‫By playing within that, we have defined the first stage, which is compile and run sonar analysis.

42
00:02:39,350 --> 00:02:41,860
‫Within this stage we have defined the steps.

43
00:02:41,870 --> 00:02:48,530
‫The steps are that we are running a shell script where we are running Maven command, which is used

44
00:02:48,530 --> 00:02:50,010
‫to run any given command.

45
00:02:50,030 --> 00:02:51,700
‫We are cleaning our code.

46
00:02:51,710 --> 00:02:54,260
‫We are then verifying our code.

47
00:02:54,260 --> 00:02:54,920
‫Verify.

48
00:02:54,950 --> 00:02:59,710
‫Syntax is used whenever we want to run the unit test cases for our source code.

49
00:02:59,720 --> 00:03:05,210
‫But currently since we have not written any unit test cases within our source code, so we will not

50
00:03:05,210 --> 00:03:07,430
‫see any code coverage on sooner code.

51
00:03:07,460 --> 00:03:11,460
‫After that we are running the sonar colon sonar syntax.

52
00:03:11,480 --> 00:03:17,290
‫Now this syntax is used to run sonar analysis on a MAVEN project.

53
00:03:17,480 --> 00:03:22,420
‫Now, sonar needs few inputs to run this sonar cloud analysis on our search code.

54
00:03:22,430 --> 00:03:27,110
‫The first is Project Key, the second is organization, third is host URL.

55
00:03:27,110 --> 00:03:34,420
‫And for this log in token, now we will define all these things within sonar cloud dashboard.

56
00:03:34,430 --> 00:03:36,740
‫So let's switch to sonar cloud dashboard.

57
00:03:36,770 --> 00:03:39,590
‫Now I am within my sonar cloud dashboard.

58
00:03:39,590 --> 00:03:44,390
‫Now let's click on this plus icon to add a new organization.

59
00:03:44,390 --> 00:03:46,280
‫I'll create new organization.

60
00:03:47,210 --> 00:03:50,180
‫Then I will create an organization manually.

61
00:03:50,180 --> 00:03:51,560
‫Let's click on this.

62
00:03:51,860 --> 00:03:54,860
‫Then I'm going to provide an organization key.

63
00:03:54,860 --> 00:03:59,510
‫I'll give it as a security guru buggy web app.

64
00:03:59,510 --> 00:04:04,070
‫I'll copy this as well as I'm going to use the same key for my project.

65
00:04:04,070 --> 00:04:06,380
‫Key, you can use a different key as well.

66
00:04:06,380 --> 00:04:12,590
‫I'll click on Continue, I'll select the free plan and I'll click on Create Organization.

67
00:04:13,070 --> 00:04:17,450
‫As you can see, our organization has been created now analyze a new project.

68
00:04:17,450 --> 00:04:18,560
‫Let's click on it.

69
00:04:18,740 --> 00:04:20,600
‫I'll provide a project key.

70
00:04:20,600 --> 00:04:25,220
‫I'll paste the same name that we have given for organization key.

71
00:04:25,250 --> 00:04:30,500
‫You can have a different project key as well, and then click on Setup Button.

72
00:04:32,730 --> 00:04:37,470
‫As you can see, our organization and Project Key is created.

73
00:04:37,470 --> 00:04:40,200
‫Let's move back to our Jenkins file.

74
00:04:40,380 --> 00:04:42,840
‫I'll paste the organization key here.

75
00:04:43,080 --> 00:04:45,300
‫I'll paste the project key here.

76
00:04:45,510 --> 00:04:50,340
‫Host We've already defined which is TPS in our cloud io.

77
00:04:50,370 --> 00:04:52,050
‫You can see the URL here.

78
00:04:52,080 --> 00:04:58,020
‫This is the host URL and the final piece of information that we need to provide this on our token.

79
00:04:58,030 --> 00:05:00,930
‫So let's generate it from Cloud Dashboard.

80
00:05:01,050 --> 00:05:03,360
‫I'll click on the profile icon on the top.

81
00:05:03,360 --> 00:05:03,960
‫Right.

82
00:05:04,290 --> 00:05:05,910
‫I'll go to my account.

83
00:05:05,910 --> 00:05:06,870
‫I'll click on it.

84
00:05:07,230 --> 00:05:09,510
‫Then I'll click on security.

85
00:05:09,660 --> 00:05:12,590
‫Then I'll generate sonar token.

86
00:05:12,600 --> 00:05:19,920
‫I'll provide the token name as well, which will be buggy token, and I'll click on Generate.

87
00:05:19,950 --> 00:05:21,570
‫Now let's copy it.

88
00:05:21,660 --> 00:05:23,100
‫I've copied the token.

89
00:05:23,130 --> 00:05:27,320
‫One thing to notice here is that you will be only able to see this token once.

90
00:05:27,330 --> 00:05:31,690
‫After you move away from this page, you will not be able to see this token.

91
00:05:31,710 --> 00:05:34,890
‫So make sure you copy it and save it somewhere secure.

92
00:05:35,280 --> 00:05:39,180
‫I'll go back to our Jenkins file and I'll paste the token here.

93
00:05:39,600 --> 00:05:44,700
‫Now, our information to run cloud on our source code has been added.

94
00:05:44,700 --> 00:05:46,430
‫Let's commit these changes.

95
00:05:46,440 --> 00:05:47,670
‫I'll scroll down.

96
00:05:48,910 --> 00:05:51,850
‫And I'll click on Commit Changes button.

97
00:05:51,850 --> 00:05:52,930
‫Let's click on it.

98
00:05:55,280 --> 00:06:02,150
‫No changes have been added to Jenkins file and we are all set up to integrate our sonar cloud within

99
00:06:02,150 --> 00:06:03,650
‫our Devsecops pipeline.

100
00:06:03,680 --> 00:06:05,840
‫Let's go back to Jenkins Dashboard.

101
00:06:05,870 --> 00:06:07,520
‫Let's click on New Item.

102
00:06:09,430 --> 00:06:10,030
‫No.

103
00:06:10,030 --> 00:06:14,430
‫Let's provide a suitable job name for our Devsecops pipeline.

104
00:06:14,440 --> 00:06:23,590
‫I'll give it as is g buggy devsecops pipeline and select the pipeline option here and I'll click on

105
00:06:23,740 --> 00:06:24,310
‫Key.

106
00:06:26,070 --> 00:06:30,480
‫Now let's scroll down and you will see this pipeline option.

107
00:06:30,480 --> 00:06:37,080
‫Under that, it is asking us to select a pipeline definition and select pipeline script from Source

108
00:06:37,080 --> 00:06:43,170
‫Code Management as our pipeline script, which is Jenkins file is present in our GitHub repo.

109
00:06:43,200 --> 00:06:46,090
‫Then I'll select the source code management as kit.

110
00:06:46,110 --> 00:06:49,800
‫Now it will ask us to provide the repository URL.

111
00:06:49,830 --> 00:06:54,000
‫I'll select the repository URL in order to select the repository URL.

112
00:06:54,030 --> 00:06:55,110
‫Click on code.

113
00:06:56,730 --> 00:07:00,360
‫Then click on code again, then copy this URL.

114
00:07:00,900 --> 00:07:04,440
‫Go back to Jenkins and paste the URL here.

115
00:07:06,150 --> 00:07:10,980
‫As you can see, it has accepted the URL now since it's a public report.

116
00:07:11,010 --> 00:07:13,400
‫We do not need to provide any credentials.

117
00:07:13,410 --> 00:07:14,660
‫Let's scroll down.

118
00:07:14,670 --> 00:07:21,030
‫Now it is asking us which branch we want to build and it is assuming that it's a master branch.

119
00:07:21,030 --> 00:07:24,390
‫But make sure that you go back to your GitHub repo.

120
00:07:24,390 --> 00:07:27,120
‫You check which branch you have selected currently.

121
00:07:27,120 --> 00:07:31,230
‫You can see it is our main branch and we only have one branch here.

122
00:07:31,230 --> 00:07:34,920
‫So I'll change the master to mean branch.

123
00:07:36,280 --> 00:07:42,010
‫And let's scroll down and then it will ask us where the script is written.

124
00:07:42,250 --> 00:07:48,520
‫Currently, the default name will be Jenkins file and we have used the same name within our source code.

125
00:07:48,520 --> 00:07:56,110
‫You can verify that it's the same name Jenkins file and we have given the same name in script path so

126
00:07:56,110 --> 00:07:57,370
‫we can save it.

127
00:07:57,370 --> 00:07:59,470
‫I'll apply it and save it.

128
00:08:00,870 --> 00:08:02,870
‫So now we can run our job.

129
00:08:02,880 --> 00:08:04,710
‫I'll click on Bill Now Button.

130
00:08:06,680 --> 00:08:09,110
‫Now the spy plane is running.

131
00:08:09,110 --> 00:08:13,860
‫As you can see, it is currently checking out the source code it has checked out.

132
00:08:13,880 --> 00:08:18,090
‫Then it is installing the tool which is MAVEN tool configuration.

133
00:08:18,110 --> 00:08:23,430
‫Now it is running sonar analysis on our source code in order to see the logs.

134
00:08:23,450 --> 00:08:26,150
‫Click on this build number, which is one.

135
00:08:27,510 --> 00:08:30,090
‫Then go to console output, click on it.

136
00:08:32,370 --> 00:08:37,980
‫And you will be able to see the logs of the build as you can see if I scroll up.

137
00:08:38,130 --> 00:08:40,950
‫So not even plugging downloaded sonar.

138
00:08:40,980 --> 00:08:45,250
‫Q And it has started running this sonar analysis on our source code.

139
00:08:45,270 --> 00:08:51,540
‫One thing to notice here is sonar cloud is just a software as a service application of sonar.

140
00:08:51,570 --> 00:08:55,710
‫Q So internally it is again using sonar itself.

141
00:08:55,710 --> 00:08:58,470
‫That's why you see sonar version mentioned here.

142
00:08:58,500 --> 00:09:04,430
‫If I scroll down, if I scroll down, you can see that our sonar cloud analysis is complete.

143
00:09:04,440 --> 00:09:10,830
‫Our sonar cloud analysis is successful and sonar cloud analysis is published on this URL and build is

144
00:09:10,830 --> 00:09:11,670
‫successful.

145
00:09:11,700 --> 00:09:15,390
‫Let's click on this URL to view the analysis.

146
00:09:15,420 --> 00:09:17,130
‫I'll open this in a new tab.

147
00:09:17,640 --> 00:09:19,260
‫I'll move to that tab.

148
00:09:20,340 --> 00:09:23,040
‫As you can see, there are 299 findings.

149
00:09:23,070 --> 00:09:27,190
‫I'll scroll down and you can see that this is the first analysis.

150
00:09:27,210 --> 00:09:29,220
‫Let's click on Mean Branch.

151
00:09:31,170 --> 00:09:34,280
‫And you can see the entire findings from sonar cloud.

152
00:09:34,290 --> 00:09:37,350
‫So there were 56 bugs in our source code.

153
00:09:37,350 --> 00:09:39,570
‫154 code smells.

154
00:09:39,600 --> 00:09:42,990
‫51 Security vulnerabilities have been identified.

155
00:09:43,020 --> 00:09:46,470
‫38 security hotspots have are ready for review.

156
00:09:46,500 --> 00:09:54,540
‫6.6% code duplication coverage is zero because as I mentioned, we still need to make some changes in

157
00:09:54,540 --> 00:09:55,410
‫order to populate.

158
00:09:55,410 --> 00:09:57,630
‫You need test code coverage on our cloud.

159
00:09:57,630 --> 00:10:04,440
‫So that's how you integrate sonar cloud within Devsecops pipeline that we implemented using Jenkins.

160
00:10:04,440 --> 00:10:06,720
‫I hope you have enjoyed this lecture.

161
00:10:06,750 --> 00:10:07,890
‫Thanks for watching.

162
00:10:07,920 --> 00:10:09,630
‫See you in the next lecture.


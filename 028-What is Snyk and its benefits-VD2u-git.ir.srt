1
00:00:00,0 --> 00:00:10,0
--- GIT.IR e-Learning Platform ---

1
00:00:00,090 --> 00:00:01,400
‫Hello, security gurus.

2
00:00:01,410 --> 00:00:02,880
‫Welcome to this new lecture.

3
00:00:02,910 --> 00:00:06,900
‫In this lecture, we're going to learn about Snake and its benefits.

4
00:00:06,900 --> 00:00:08,870
‫So let's see what is Snake?

5
00:00:08,880 --> 00:00:14,370
‫Snake is an organization that develops security tools, which are software as a service.

6
00:00:14,370 --> 00:00:17,640
‫It means that those are available on the cloud.

7
00:00:17,640 --> 00:00:25,020
‫And those security tools help us to secure source code, which is written by the developers within the

8
00:00:25,020 --> 00:00:25,890
‫organization.

9
00:00:25,890 --> 00:00:31,200
‫These security tools help us to identify security issues within this source code.

10
00:00:31,200 --> 00:00:36,690
‫It is also called as sast or static application security testing.

11
00:00:36,690 --> 00:00:40,140
‫Next is open source or third party libraries.

12
00:00:40,140 --> 00:00:47,410
‫So snake security tools help us to identify security issues within open source or third party libraries.

13
00:00:47,410 --> 00:00:49,790
‫A very common example is Log Forge.

14
00:00:49,860 --> 00:00:56,850
‫A very high severity issue was recently identified, so now Snake has released a patch for that security

15
00:00:56,850 --> 00:01:03,210
‫issue within Log Forge and now it identifies all those security issues within Log Forge as well.

16
00:01:03,210 --> 00:01:05,010
‫So that is one example.

17
00:01:05,010 --> 00:01:10,110
‫This kind of scan is also called this software composition analysis scan.

18
00:01:10,110 --> 00:01:14,700
‫Third is continuous snake security tools help us to secure containers.

19
00:01:14,700 --> 00:01:20,160
‫Containers can be Docker containers or it can be Kubernetes pods as well.

20
00:01:20,160 --> 00:01:23,340
‫Then next and last one is infrastructure escort.

21
00:01:23,340 --> 00:01:27,060
‫So nowadays most of the applications are moving to cloud.

22
00:01:27,060 --> 00:01:30,960
‫And in order to build cloud infrastructure, we are writing code.

23
00:01:30,960 --> 00:01:37,350
‫For example, for eight years we are writing cloud formation templates and also we are using TerraForm

24
00:01:37,350 --> 00:01:43,020
‫scripts to create infrastructure within it as Azure and other cloud platforms.

25
00:01:43,020 --> 00:01:50,940
‫So Snake helps us to identify configuration issues within the code written in TerraForm and other cloud

26
00:01:50,940 --> 00:01:52,200
‫platform templates.

27
00:01:52,200 --> 00:01:58,320
‫So this was the definition of Snake and its benefits by a security guru.

28
00:01:58,320 --> 00:02:00,560
‫I hope you have enjoyed this lecture.

29
00:02:00,570 --> 00:02:01,770
‫Thanks for watching.

30
00:02:01,800 --> 00:02:03,630
‫See you in the next lecture.


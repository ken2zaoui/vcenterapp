1
00:00:00,0 --> 00:00:10,0
--- GIT.IR e-Learning Platform ---

1
00:00:00,150 --> 00:00:01,560
‫Hello, security gurus.

2
00:00:01,560 --> 00:00:03,210
‫Welcome to this new lecture.

3
00:00:03,240 --> 00:00:07,070
‫In this lecture, we are going to create an account with Snake.

4
00:00:07,080 --> 00:00:12,750
‫Snake is used to perform software completion analysis scan on our source code.

5
00:00:12,750 --> 00:00:19,470
‫There are other capabilities of Snake as well, but we are going to use Snake specifically for software

6
00:00:19,470 --> 00:00:21,300
‫composition analysis scan.

7
00:00:21,300 --> 00:00:28,440
‫So software composition analysis can is used to identify security issues within third party libraries

8
00:00:28,440 --> 00:00:30,180
‫that are present in our source code.

9
00:00:30,180 --> 00:00:38,220
‫For an example, Log Forge is a third party library that is used in many Springwood projects, so Snake

10
00:00:38,220 --> 00:00:42,230
‫will identify all security issues present with log for J.

11
00:00:42,240 --> 00:00:46,200
‫So I hope you now understand why we are going to use Snake.

12
00:00:46,200 --> 00:00:48,180
‫So let's jump into the action.

13
00:00:48,180 --> 00:00:56,250
‫I am on Snake homepage and currently the website is Snake Dot IO on which we are going to create an

14
00:00:56,250 --> 00:00:56,730
‫account.

15
00:00:56,730 --> 00:01:00,390
‫In order to create an account, we need to click on signup button.

16
00:01:00,390 --> 00:01:01,710
‫So let's click on it.

17
00:01:03,830 --> 00:01:10,730
‫Now once we click on signup button, we are routed to this page where it says that we can create an

18
00:01:10,730 --> 00:01:15,160
‫account with snake free, of course, and there is no credit card required.

19
00:01:15,170 --> 00:01:16,940
‫Now there are multiple options.

20
00:01:16,940 --> 00:01:20,690
‫In order to create an account with Snake, we can use GitHub account.

21
00:01:20,690 --> 00:01:27,680
‫We can use Bitbucket account, we can use Google account as your Active Directory or doc account as

22
00:01:27,680 --> 00:01:28,100
‫well.

23
00:01:28,100 --> 00:01:33,090
‫And Snake provides an option for companies single sign on as well.

24
00:01:33,110 --> 00:01:39,740
‫But now, since I already have a GitHub account, I'm going to use GitHub for creating an account with

25
00:01:39,740 --> 00:01:40,220
‫Snake.

26
00:01:40,220 --> 00:01:42,530
‫So let's click on GitHub button.

27
00:01:44,640 --> 00:01:45,300
‫No.

28
00:01:45,300 --> 00:01:48,930
‫We need to enter their GitHub username and password.

29
00:01:48,930 --> 00:01:55,020
‫I have entered my GitHub username which is a security guru and my password for GitHub account.

30
00:01:55,020 --> 00:01:56,970
‫Let's click on sign in button.

31
00:02:00,770 --> 00:02:01,370
‫No.

32
00:02:01,400 --> 00:02:07,820
‫It is redirecting us to the authorized application, which in this case is snake.

33
00:02:07,850 --> 00:02:12,210
‫Now, as you can see, our account has been created.

34
00:02:12,230 --> 00:02:19,250
‫Account name is a security guru and we have successfully created an account with Snake.

35
00:02:19,250 --> 00:02:25,580
‫In the next lecture we will start integrating Snake with that GCP Devsecops pipeline.

36
00:02:25,580 --> 00:02:28,280
‫So stay tuned and thanks for watching.

